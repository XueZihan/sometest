abstract class A {
  def apply(a:Int, b:Int):Int
}

class B extends A {
  def apply(a:Int, b:Int):Int = a+b
}


class C extends A {
  def apply(a:Int, b:Int):Int = a*b
}

class D {
  def apply(A: A) = A(1,2)
}

val a= new A {
  def apply(a:Int, b:Int):Int =a-b
}

val b = new B
val c = new C
val d = new D
d(a)
d(b)
d(c)


abstract class E {
  def apply(a:Int, b:Int):Int
  final def push() = {
    val me = this
    new E {
      def apply(a:Int, b:Int):Int = me.apply(a,b)
    }
  }
}

class F extends E {
   def apply(a:Int, b:Int):Int=a+b
}
