/*
val config = Class.forName(s"$configProjectName.$configClassName").newInstance.asInstanceOf[Config]
val world = config.toInstance
val paramsFromConfig: Parameters = Parameters.root(world)
*/


case class TileLinkParameters(
    //coherencePolicy: CoherencePolicy,
    nManagers: Int,
    nCachingClients: Int,
    nCachelessClients: Int,
    maxClientXacts: Int,
    maxClientsPerPort: Int,
    maxManagerXacts: Int,
    dataBits: Int,
    dataBeats: Int = 4,
    overrideDataBitsPerBeat: Option[Int] = None
    ) {
  println("Creat TileLinkParameters") 
  val nClients = nCachingClients + nCachelessClients
  val writeMaskBits: Int  = ((dataBits / dataBeats) - 1) / 8 + 1
  val dataBitsPerBeat: Int = overrideDataBitsPerBeat.getOrElse(dataBits / dataBeats)
}

class Field[T]
case object XLen extends Field[Int]
case object TagBits extends Field[Int]
case object PgLevelBits extends Field[Int]
case object PgLevel0 extends Field[Int]
case object PgLevel1 extends Field[Int]
case object PgLevel2 extends Field[Int]
case object NSets extends Field[Int]
case object NWays extends Field[Int]
case object RowBits extends Field[Int]
case object RAMSize extends Field[Int]
case object RAMHere extends Field[Int]
case object RAMSite extends Field[Int]
case object CacheBlockBytes extends Field[Int]
case object CacheName extends Field[String]
case object CacheBlockOffsetBits extends Field[Int]

case object TLId extends Field[String]
case class TLKey(id: String) extends Field[TileLinkParameters] { 
     printf("Creat TLKey id = %s\n", id) 
}


abstract class Ex[T]
final case class ExLit[T](value:T) extends Ex[T]
final case class ExAnd(a:Ex[Boolean], b:Ex[Boolean]) extends Ex[Boolean]

object Ex {
  def eval[T](e:Ex[T]):T = e match {
    case ExLit(v) => v.asInstanceOf[T]
    case ExAnd(a,b) => eval(a) && eval(b)
  }
}

abstract class View {
  protected val deftSite: View
  def apply[T](pname:Any, site:View):T
  final def apply[T](pname:Any):T = {
             println("pname:Any"); apply[T](pname, deftSite)
  }
  final def apply[T](field:Field[T]):T = {
             println("field:Field[T]");  apply[T](field.asInstanceOf[Any], deftSite)
  }
}

abstract class _Lookup {
  def apply[T](pname:Any, site:View):Ex[T]
}

object World { type TopDefs = (Any,View,View) => Any}

abstract class World(
    topDefs: World.TopDefs
  ) {

  abstract class _View extends View {
    val look: _Lookup

    def apply[T](pname:Any, site:View):T = {
       _eval(look(pname, site).asInstanceOf[Ex[T]])
    }
  }

  def _eval[T](e:Ex[T]):T = { Ex.eval(e) }

  def _siteView(look:_Lookup):View = {
    val _look = look
    println("_siteView: new _View")
    new _View {
      val look = _look
      val deftSite = this
    }
  }

   def _otherView(look:_Lookup, deftSite:View):View = {
    val _look = look
    val _deft = deftSite
    println("_otherView: new _View")
    new _View {
      val look = _look
      val deftSite = _deft
    }
  }

  def _topLook():_Lookup = {
    class TopLookup extends _Lookup {
      def apply[T](pname:Any, site:View):Ex[T] =  {
        val here = _otherView(this, site)
        (
          try topDefs(pname, site, here)
          catch {
             case _ => println("Wrong");
          }
       ) match {
         //case ex:Ex[T @unchecked] => _bindLet[T](pname,ex)
         case lit => ExLit(lit.asInstanceOf[T])
         //case _   => println("no match") 
       }
      }
    }
    println("new TopLookup")
    new TopLookup
 }
}

class Instance(
    topDefs: World.TopDefs
  ) extends World(topDefs) {}

class Config(val topDefinitions: World.TopDefs = { (a,b,c) => 1}) {
  println("Config")
  def toInstance = new Instance(this.topDefinitions)
  def this(that: Config) = {this(that.topDefinitions);println("this(that: Config)")}
  //def this(that: Config) = {println("this(that: Config)");this(that.topDefinitions)}
  def ++(that: Config) = {
    new Config(this.addDefinitions(that.topDefinitions))
  }
  def addDefinitions(that: World.TopDefs): World.TopDefs = {
    println("addDefinitions")
    (pname,site,here) => {
      println("addDefinitions have a try")
      try this.topDefinitions(pname, site, here)
      catch {
        case _ => {println("catch _"); that(pname, site, here) }
      }
    }
  }
}

class BaseConfig extends Config (topDefinitions = { 
   (pname,site,here) => 
     println("BaseConfig");
     type PF = PartialFunction[Any,Any]
     def findBy(sname:Any):Any = here[PF](site[Any](sname))(pname)
      pname match {
       case XLen => {println("XLen"); 19}
       
       case PgLevelBits => { println("PgLevelBits"); site(XLen)}
       case PgLevel0 => { println("PgLevel0"); 156}
       case PgLevel1 => { println("PgLevel1"); site(PgLevel0)}
       case PgLevel2 => { println("PgLevel2"); site(PgLevel1)}
       case RAMSize => 40  // 512 MB
       case CacheBlockBytes => { println("CacheBlockBytes"); 64 }
       case CacheBlockOffsetBits => here(CacheBlockBytes)+1

       case NSets      => {println("NSets"); findBy(CacheName)}
       case RowBits    => {println("RowBits"); findBy(CacheName)}
       case CacheName  => {println("CacheName"); "L1I"}
       case TLId       => {println("TLId"); "L1toL2"}
       case "L1I" => {
            println("\"L1I\"");
            {
              case NSets => 32
              case RowBits => site(TLKey(site(TLId))).dataBitsPerBeat
            }
       }:PF
       case "L1D" => { println("\"L1D\""); 65}
       case TLKey("L1toL2") => {
               println("TLKey(\"L1toL2\")");
               TileLinkParameters(
                 //coherencePolicy = new MESICoherence(site(L2DirectoryRepresentation)),
                 nManagers =1,
                 nCachingClients = 2,
                 nCachelessClients = 3,
                 maxClientXacts = 4,
                 maxClientsPerPort = 5,
                 maxManagerXacts = 6, // acquire, release, writeback
                 dataBits = site(CacheBlockBytes)*8,
                 dataBeats = 8
              )
       }
  }})

class HereConfig extends Config (topDefinitions = {
  (pname,site,here) => 
   println("HereConfig");
   pname match {
    case RAMSize => 38  // 512 MB
    case RAMHere => here(RAMSize)
    case RAMSite => site(RAMSize)
  }
 }
)

//class AllConfig extends Config(new BaseConfig ++ new HereConfig)
class AllConfig extends Config(new HereConfig ++ new BaseConfig)

final class Parameters(
    private val _world: World,
    private val _look: _Lookup
  ) {

  private def _site() = _world._siteView(_look)

  def apply[T](field:Any):T =
    _world._eval(_look(field, _site())).asInstanceOf[T]

  def apply[T](field:Field[T]):T =
    _world._eval(_look(field, _site())).asInstanceOf[T]

}

object Parameters {
  def root(w:World) = {
    new Parameters(w, w._topLook())
  }
}


val config = new AllConfig
//val config = new BaseConfig
val world = config.toInstance
val paramsFromConfig: Parameters = Parameters.root(world)
val p = paramsFromConfig
p(PgLevel2)
p(CacheBlockOffsetBits)
p(CacheName)
p(NSets)
p(TLKey("L1toL2"))
p(RowBits)
p(RAMSize)
p(RAMHere)
p(RAMSite)

