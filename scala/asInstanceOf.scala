val i:Any = 12
i.asInstanceOf[Int]
i.asInstanceOf[Double]
i.asInstanceOf[Int].asInstanceOf[Double]



class TField[F,S,T]

case object IFDTF extends TField[Int,Float,Double]
case object IIITF extends TField[Int,Int,Int]
case object DDDTF extends TField[Double,Double,Double]
case object SSSTF extends TField[String,String,String]


def getvalue[F,S,T](tfield:TField[F,S,T]) = {val temp = (tfield match {
                                        case IFDTF => {println("IFDTF"); 119}
                                        case IIITF => {println("IIITF"); 109}
                                        case DDDTF => {println("IIITF"); 10.9}
                                        case SSSTF => {println("IIITF"); "ASW"}
                                       })
                                        println(temp.getClass)
                                        //temp.asInstanceOf[T] //wrong
                                        temp.asInstanceOf[Int] //no efect
                                        println(temp.getClass)
                                        temp
                                       }
getvalue(IFDTF)
getvalue(IIITF)
getvalue(DDDTF)
getvalue(SSSTF)

def getLvalue[F,S,T](tfield:List[TField[F,S,T]])  = {val temp = (tfield match {
                                        case List(IFDTF) => {println("IFDTF"); 119}
                                        //case List(IIITF) => {println("IIITF"); 109}
                                        //case List(DDDTF) => {println("IIITF"); 10.9}
                                        //case List(SSSTF) => {println("IIITF"); "ASW"}
                                       }).asInstanceOf[F] //wrong
                                       //}).asInstanceOf[Double] //right
                                        println(temp.getClass)
                                        //temp.asInstanceOf[T] //wrong
                                        println(temp.getClass) 
                                        temp
                                       }

getLvalue(List(IFDTF))
getLvalue(List(IIITF))
getLvalue(List(DDDTF))
getLvalue(List(SSSTF))
