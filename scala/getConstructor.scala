/*
  val gen = () =>
  Class.forName(s"$projectName.$topModuleName")
    .getConstructor(classOf[cde.Parameters])
    .newInstance(paramsFromConfig)
    .asInstanceOf[Module]
*/


/*

package Mainn {

class Person(ID:Int=0) {
        private var age: Int =0;
        private var name: String="asd";
        
        println(ID);
        def Person(name: String, age:Int) {
            this.age = age;
            this.name = name;
            println("构造函数Person(有参数)执行");
        }

        def Person() {
            println("构造函数Person(无参数)执行");
        } 
}

}

*/

package Mainn;
 
import java.lang.reflect.Constructor;


class Person(ID:Int=0) {
        private var age: Int =0;
        private var name: String="asd";
        
        println(ID);
        def Person(name: String, age:Int) {
            this.age = age;
            this.name = name;
            println("构造函数Person(有参数)执行");
        }

        def Person() {
            println("构造函数Person(无参数)执行");
        } 
}

object Main {
    def main(args: Array[String]) {
        //当我不想 newInstance初始化的时候执行空参数的构造函数的时候
        //可以通过字节码文件对象方式 getConstructor(paramterTypes) 获取到该构造函数
        val classname ="Mainn.Person";
        //寻找名称的类文件，加载进内存 产生class对象
        val cl=Class.forName(classname);
              println("1");
        //获取到Person(String name,int age) 构造函数
              println("2");
        val con1=cl.getConstructor(classOf[Int]);
              println("3");
        //通过构造器对象 newInstance 方法对对象进行初始化 有参数构造函数
        val obj1=con1.newInstance(23);
             println("4");
        val obj11=obj1.asInstanceOf[Person]
             println("5");
        obj11.Person("神奇的我",12)
        //val obj2=con.newInstance("神奇的我",12);

    }
}
