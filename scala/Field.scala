class Field[T]
class TField[F,S,T]
case class TLKey(id: String) extends Field[Int]


case object IntF extends Field[Int]
case object DoubleF extends Field[Double]
case object StringF extends Field[String]
case object IOTagBits extends Field[Int]
case object UseHost extends Field[Boolean]
object TLKey01 extends TLKey("01")
object TLKey01 extends TLKey("02")




List(UseHost) collect { case i: Field[Boolean] =>1 }
List(UseHost,2) collect { case i: Field[Boolean] =>1 }
List(UseHost) collect { case i: Field[Int] =>1 }                 //wrong!!!?????
List(UseHost,IOTagBits) collect { case i: Field[Int] =>1; case i: Field[Boolean] =>11; }
List(UseHost,IOTagBits,TLKey01) collect { case UseHost =>1; case IOTagBits =>11; case TLKey01 => 101}
List(UseHost,IOTagBits) collect { case i: UseHost =>1; case i: IOTagBits =>11; }  //wrong
List(UseHost,2) collect { case i: Field[Int] =>1 }

def getvalue[T](field:Field[T]) :T = (field match {
                                      case IntF => {println("IntF"); 19}
                                      case DoubleF => { println("DoubleF"); 1.3}
                                      case StringF => { println("StringF"); "Hello"}
                                     }).asInstanceOf[T] 


