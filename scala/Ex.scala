abstract class Ex[T]
final case class ExLit[T](value:T) extends Ex[T]
final case class ExSub(a:Ex[Int], b:Ex[Int]) extends Ex[Int]


def eval[T](e:Ex[T]) :T = e match {
         case ExLit(v) => v.asInstanceOf[T];
         case ExSub(a,b) => eval(a) - eval(b);
       }


ExSub(ExLit(10),ExLit(1))

def eval[T](e:Ex[T]) = e match {case ExLit(v) => v.asInstanceOf[T]+1;}
