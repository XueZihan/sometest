int main(void)
{
    int a[] = {0x01020304,0x7e3};
    int *b = a;
    char *c = (char*)&a[0];
    printf("b+0:%d\n",*(b+0));
    printf("b+1:%d\n",*(b+1));
    printf("c+0:%d\n",*(c+0));
    printf("c+1:%d\n",*(c+1));
    printf("c+2:%d\n",*(c+2));
    printf("c+3:%d\n",*(c+3));
    printf("c+4:%d\n",*(c+4));
    printf("c+5:%d\n",*(c+5));
    printf("c+6:%d\n",*(c+6));
    printf("c+7:%d\n",*(c+8));
    printf("c+8:%d\n",*(c+8));
    return 0;
}
