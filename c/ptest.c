#include <stdint.h>
#include <stdio.h>

 
volatile uint64_t get_bram_base() {
 //return (uint64_t *)(0x880000000);
  return (0x880000000);
}

 
int max(int x, int y)
{
    return x > y ? x : y;
}
 
int main(void)
{

  unsigned int i=0;
  unsigned int *raddr0=0;
  unsigned int *raddr1 = get_bram_base();
  int RollNum[30][4];
  int (*p)[4]=RollNum;  //p被声明为一个指向一个4元素（int类型）数组的指针，
  int *q[5]; //而q被声明为一个包含5个元素（int类型的指针）的数组
  printf("p= %x \n", p++);
  printf("p++= %x \n", p++);
  printf("p++= %x \n", p++);
  printf("p++= %x \n", p++);
  for(i=0; i<10; i++) {
    printf("addr0= %8x addr1= %8x \n", raddr0, raddr1);
    raddr0=get_bram_base()+i;
    raddr1=raddr1+1;    
  }

{
int a;
	int *p;
	p = &a;
	scanf("%d", p);
	printf("p = %d\n", p);
	printf("*p = %d\n", *p);
	printf("&*p = %d\n", &*p);
	printf("*&*p = %d\n", *&*p);
	printf("a = %d\n", a);
	printf("&a = %d\n", &a);
	printf("*&a = %d\n", *&a);

}

}

